package net.lecousin.microservices.commons.mapping;

import net.lecousin.microservices.commons.reflection.ResolvedType;
import net.lecousin.microservices.commons.utils.OptionalNullable;

public interface GenericMapper {

	boolean canMap(ResolvedType from, ResolvedType to);
	
	OptionalNullable<Object> map(ResolvedType sourceType, Object sourceValue, ResolvedType targetType);
	
}
