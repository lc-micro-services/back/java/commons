package net.lecousin.microservices.commons.validation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;

import net.lecousin.microservices.commons.reflection.ClassProperty;
import net.lecousin.microservices.commons.reflection.ObjectPropertiesCache;
import net.lecousin.microservices.commons.reflection.ReflectionUtils;
import net.lecousin.microservices.commons.validation.annotations.Ignore;
import net.lecousin.microservices.commons.validation.annotations.Mandatory;
import net.lecousin.microservices.commons.validation.exceptions.MissingRequiredPropertyException;
import net.lecousin.microservices.commons.validation.exceptions.ValidationException;

public class Validator<T> {
	
	private Map<ValidationContext, List<ValidatorChain>> validatorsByContexts = new HashMap<>();

	public Validator(Class<T> type) {
		var properties = ReflectionUtils.getAllProperties(type);
		for (var context : ValidationContext.values()) {
			List<ValidatorChain> list = new LinkedList<>();
			validatorsByContexts.put(context, list);
			for (var property : properties.values()) {
				List<Annotation> annotations = filterAnnotationsForContext(property.getAnnotations(), context);
				if (annotations.isEmpty()) continue;
				list.add(createValidator("", property, properties, annotations));
			}
		}
	}
	
	private List<Annotation> filterAnnotationsForContext(List<Annotation> annotations, ValidationContext context) {
		return annotations.stream().filter(a -> {
			ValidationContext[] contexts = getContexts(a);
			if (contexts == null) return false;
			if (contexts.length == 0) return true;
			return ArrayUtils.contains(contexts, context);
		}).toList();
	}
	
	private ValidationContext[] getContexts(Annotation a) {
		try {
			Method m = a.annotationType().getMethod("context");
			if (m.getReturnType().equals(ValidationContext[].class)) return (ValidationContext[]) m.invoke(a);
			return null;
		} catch (Exception e) {
			return null;
		}
	}
	
	private ValidatorChain createValidator(String containerPath, ClassProperty property, Map<String, ClassProperty> properties, List<Annotation> annotations) {
		ValidatorChain chain = new ValidatorChain();
		Ignore ignore = getAnnotation(Ignore.class, annotations);
		if (ignore != null && ignore.value())
			return chain;
		Mandatory mandatory = getAnnotation(Mandatory.class, annotations);
		if (mandatory != null)
			if (mandatory.value()) {
				chain.add((obj, cache) -> {
					if (cache.get(obj, property) == null)
						throw new MissingRequiredPropertyException(containerPath + property.getName());
					return true;
				});
			} else {
				chain.add((obj, cache) -> cache.get(obj, property) != null);
			}
		// TODO continue
		return chain;
	}
	
	@SuppressWarnings("unchecked")
	private <A extends Annotation> A getAnnotation(Class<A> annotationType, List<Annotation> annotations) {
		return (A) annotations.stream().filter(a -> annotationType.equals(a.getClass())).findAny().orElse(null);
	}
	
	public void validate(T object, ValidationContext context) throws ValidationException {
		ObjectPropertiesCache cache = new ObjectPropertiesCache();
		for (ValidatorChain chain : validatorsByContexts.get(context))
			chain.validate(object, cache);
	}

}
