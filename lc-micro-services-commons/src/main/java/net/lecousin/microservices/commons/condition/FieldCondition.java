package net.lecousin.microservices.commons.condition;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FieldCondition implements Condition {

	private static final long serialVersionUID = 1L;
	
	private String field;
	private ValueCondition value;
	
	@Override
	public Condition accept(ConditionVisitor visitor) {
		return visitor.visit(this);
	}

}
