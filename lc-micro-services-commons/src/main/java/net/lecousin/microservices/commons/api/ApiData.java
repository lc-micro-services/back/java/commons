package net.lecousin.microservices.commons.api;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import net.lecousin.microservices.commons.validation.ValidationContext;
import net.lecousin.microservices.commons.validation.annotations.Ignore;
import net.lecousin.microservices.commons.validation.annotations.Mandatory;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class ApiData implements Serializable {

	private static final long serialVersionUID = 1L;

	@Mandatory(value = true, context = ValidationContext.UPDATE)
	@Ignore(value = true, context = ValidationContext.CREATION)
	private String id;
	
	@Mandatory(value = true, context = ValidationContext.UPDATE)
	@Ignore(value = true, context = ValidationContext.CREATION)
	private long version;
	
}
