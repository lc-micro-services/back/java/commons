package net.lecousin.microservices.commons.mapping;

import java.util.List;

import net.lecousin.microservices.commons.reflection.ResolvedType;

public interface TypeConverter {

	List<ResolvedType> canConvert(ResolvedType sourceType, ResolvedType finalType);
	
	Object doConvert(ResolvedType sourceType, Object sourceValue, ResolvedType targetType);
	
}
