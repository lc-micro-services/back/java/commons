package net.lecousin.microservices.commons.reflection;

import java.util.Optional;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

public interface ResolvedType {
	
	@AllArgsConstructor
	@Getter
	@EqualsAndHashCode
	class SingleClass implements ResolvedType {
		private final Class<?> singleClass;
	}
	
	@AllArgsConstructor
	@Getter
	@EqualsAndHashCode
	class Bounded implements ResolvedType {
		private final ResolvedType[] lowerBounds;
		private final ResolvedType[] upperBounds;
	}
	
	@AllArgsConstructor
	@Getter
	@EqualsAndHashCode
	class Parameterized implements ResolvedType {
		private final Class<?> base;
		private final ResolvedType[] parameters;
	}
	
	@AllArgsConstructor
	@Getter
	@EqualsAndHashCode
	class Array implements ResolvedType {
		private final ResolvedType componentType;
	}
	
	static Optional<Class<?>> getRawClass(ResolvedType type) {
		if (type instanceof SingleClass c) return Optional.of(c.getSingleClass());
		if (type instanceof Parameterized p) return Optional.of(p.getBase());
		return Optional.empty();
	}
	
}
