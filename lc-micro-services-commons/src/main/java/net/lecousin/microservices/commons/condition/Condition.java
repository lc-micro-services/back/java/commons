package net.lecousin.microservices.commons.condition;

import java.io.Serializable;
import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.DEDUCTION)
@JsonSubTypes({
	@JsonSubTypes.Type(ConditionAnd.class),
	@JsonSubTypes.Type(ConditionOr.class),
})
public interface Condition extends Serializable {

	Condition accept(ConditionVisitor visitor);
	
	static ConditionAnd and(Condition... conditions) {
		return new ConditionAnd(Arrays.asList(conditions));
	}
	
	static ConditionOr or(Condition... conditions) {
		return new ConditionOr(Arrays.asList(conditions));
	}
	
	static FieldCondition field(String fieldName, ValueCondition.Operator operator, Serializable value) {
		return new FieldCondition(fieldName, new ValueCondition(operator, value));
	}
	
}
