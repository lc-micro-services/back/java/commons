package net.lecousin.microservices.commons.api;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PageRequest {

	private Integer page;
	private Integer pageSize;
	private List<Sort> sort;
	private boolean withTotal;
	
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Sort {
		private String field;
		private SortOrder order;
	}
	
	public enum SortOrder {
		ASC,
		DESC,
	}
	
	public void forcePaging(int size) {
		if (page == null)
			this.page = 0;
		if (pageSize == null || pageSize < size)
			pageSize = size;
	}
	
}
