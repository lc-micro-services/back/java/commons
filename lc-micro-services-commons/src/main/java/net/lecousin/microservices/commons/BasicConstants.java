package net.lecousin.microservices.commons;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class BasicConstants {

	public static final int DECIMAL_RADIX = 10;
	
}
