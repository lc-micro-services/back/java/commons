package net.lecousin.microservices.commons.condition;

public interface ConditionVisitor {

	Condition visit(ConditionAnd and);
	
	Condition visit(ConditionOr or);
	
	Condition visit(FieldCondition field);
	
}
