package net.lecousin.microservices.commons.validation.exceptions;

public class MissingRequiredPropertyException extends ValidationException {

	private static final long serialVersionUID = 1L;

	public MissingRequiredPropertyException(String propertyPath) {
		super("Missing required property: " + propertyPath);
	}
	
}
