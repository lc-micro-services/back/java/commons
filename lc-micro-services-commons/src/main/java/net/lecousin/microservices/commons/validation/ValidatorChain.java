package net.lecousin.microservices.commons.validation;

import java.util.LinkedList;
import java.util.List;

import net.lecousin.microservices.commons.reflection.ObjectPropertiesCache;
import net.lecousin.microservices.commons.validation.exceptions.ValidationException;

class ValidatorChain {

	private List<ChainableValidator> chain = new LinkedList<>();
	
	void add(ChainableValidator validator) {
		chain.add(validator);
	}
	
	void validate(Object object, ObjectPropertiesCache cache) throws ValidationException {
		for (ChainableValidator validator : chain)
			if (!validator.validate(object, cache))
				return;
	}
	
	public interface ChainableValidator {
		
		boolean validate(Object object, ObjectPropertiesCache cache) throws ValidationException;
		
	}
}
