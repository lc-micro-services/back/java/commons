package net.lecousin.microservices.commons.condition;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ValueCondition implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Operator operator;
	private Serializable value;
	
	public enum Operator {
		EQ,
		NOT_EQ,
		LESS,
		LESS_OR_EQ,
		GREATER,
		GREATER_OR_EQ,
		IN,
		NOT_IN
	}
	
}
