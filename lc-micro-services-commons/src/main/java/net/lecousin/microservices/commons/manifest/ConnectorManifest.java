package net.lecousin.microservices.commons.manifest;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.lecousin.microservices.commons.mapping.Mappers;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class ConnectorManifest {

	private String name;
	private String impl;
	private Map<String, String> stack = new HashMap<>();
	private Map<String, Dependency> dependencies = new HashMap<>();
	
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Dependency {
		private String image;
		private String version;
		private List<Integer> exposedPorts;
		private Map<String, String> testEnvironment;
		private Map<String, String> testProperties;
	}
	
	public static List<ConnectorManifest> load() {
		List<ConnectorManifest> list = new LinkedList<>();
		try {
			var urls = MicroServiceManifest.class.getClassLoader().getResources("META-INF/connector.yaml");
			while (urls.hasMoreElements()) {
				URL url = urls.nextElement();
				try (var input = url.openStream()) {
					list.add(load(input));
				} catch (Exception e) {
					log.error("Error parsing micro-service manifest {}", url, e);
				}
			}
		} catch (Exception e) {
			log.error("Error retrieving micro-services manifests", e);
		}
		return list;
	}
	
	public static ConnectorManifest load(InputStream input) throws IOException {
		return Mappers.YAML_MAPPER.readValue(input, ConnectorManifest.class);
	}
}
