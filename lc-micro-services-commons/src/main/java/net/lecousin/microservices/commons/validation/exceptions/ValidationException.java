package net.lecousin.microservices.commons.validation.exceptions;

import net.lecousin.microservices.commons.api.exceptions.BadRequestException;

public class ValidationException extends BadRequestException {

	private static final long serialVersionUID = 1L;

	public ValidationException(String message) {
		super(message);
	}
	
}
