package net.lecousin.microservices.commons.condition;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConditionOr implements Condition {

	private static final long serialVersionUID = 1L;
	
	private List<Condition> or;
	
	@Override
	public Condition accept(ConditionVisitor visitor) {
		return visitor.visit(this);
	}

}
